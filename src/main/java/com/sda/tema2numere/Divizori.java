package com.sda.tema2numere;

import java.io.*;

public class Divizori {

    //Citesc numerele de verificat din fisier si le introduc intr-un array
    public String[] sir(String patch) {
        File file = new File(patch);
        String[] numere = new String[2];

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);

            String linie = reader.readLine();
            numere = linie.split(" ");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return numere;
    }

    // Verific cati divizori are fiecare numar din fisier si formez un alt array cu numarul de divizori
    //teteteteteste
    public int[] divizori(String[] numere) {
        int nrDivizoriPozitia0 = 0;
        int nrDivizoriPozitia1 = 0;
        int[] divizori = new int[Integer.valueOf(numere.length)];
        for (int i = 1; i <= Integer.valueOf(numere[0]); i++) {
            if ((Integer.valueOf(numere[0])) % i == 0) {
                nrDivizoriPozitia0++;
                divizori[0] = nrDivizoriPozitia0;
            }
        }
        for (int i = 1; i <= Integer.valueOf(numere[1]); i++) {
            if ((Integer.valueOf(numere[1])) % i == 0) {
                nrDivizoriPozitia1++;
                divizori[1] = nrDivizoriPozitia1;
            }
        }
        return divizori;
    }

    //Scriu rezultatele in alt fisier, in functie de numarul de divizori ai celor 2 numere citite initial
    public void scriuDivizoriiInFisier(int[] divizori) {
        File file = new File("C:\\Users\\vasile\\Desktop\\divizori.txt");

        try {
            FileWriter writer = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            int divizoriEgali = 0;
            for (int i = 0; i < divizori.length; i++) {
                if (divizori[0] == divizori[1]) {
                    divizoriEgali = divizori[0];
                }
            }
            if (divizoriEgali != 0) {
                bufferedWriter.write("Numarul de divizori ai celor 2 numere citite este acelasi, respectiv: " + divizori[0]);
            } else {
                bufferedWriter.write("Numarul de divizori ai celor doua numere citite sunt diferiti, respectiv " + divizori[0] + " pentru primul numar citit si " + divizori[1] + " pentru al doilea numar citit");
            }
            bufferedWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
